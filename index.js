/*jshint esversion: 6 */
/* jshint node: true */

'use strict';
const express = require('express');
const bodyParser = require('body-parser');
const sequelize = require('./sequelize');
const Brand = sequelize.Brand;
const BrandUrl = sequelize.BrandUrl;
const Business = sequelize.Business;
const WhereToBuy = sequelize.WhereToBuy;
const Customer = sequelize.Customer;
const Faq = sequelize.Faq;

const app = express();
app.use(bodyParser.json());

const port = 3000;
app.listen(port, () => {
  console.log(`running on http://localhost:${port}`);
});


app.post('/api/brands', (req, res) => {
  Brand.create(req.body)
    .then(user => res.json(user));
});

app.get('/api/brands', (req, res) => {
  console.log("here");
  Brand.findAll({include : [BrandUrl]}).then(brands => res.json(brands));
});

app.get('/api/biz', (req, res) => {
  // Business.findAll().then(businesses => res.json(businesses));
  Business.findAll().then(brands => res.json(brands));

});


app.get('/api/brand/:id', (req, res) => {
  Brand.find({ where: {id: req.params.id}, include: [BrandUrl]}).then(brand => res.json(brand));
});

app.post('/api/brand_urls', (req, res) => {
  const body = req.body;

  Brand.findById(body.bodyId)
    .then(() => BrandUrl.create(body));
});

app.get('/api/brand_url/:brandId?', (req,res) => {
  let query;
  if(req.params.brandId){
    query = BrandUrl.findAll({ include: [
      { attributes: [], model: Brand, where: {id: req.params.brandId} }
    ]});
  } else {
    query = BrandUrl.findAll();
  }
  return query.then(brand_urls => res.json(brand_urls));
});

app.get('/api/brand_urls', (req, res) => {
  console.log("KASJD", BrandUrl.options.classMethods.associate);

  BrandUrl.findAll().then(brandUrls => res.json(brandUrls));
});

app.get('/api/where_to_buy', (req, res) => {
  WhereToBuy.findAll().then(whereToBuy => res.json(whereToBuy));
});

app.get('/api/where_to_buy/:brand_name?', (req, res) => {
  let query;
  if(req.params.brand_name){
    query = WhereToBuy.findAll({ include: [
      { model: Brand, where: {name: req.params.brand_name} }
    ]});
  } else {
    query = WhereToBuy.findAll();
  }
  return query.then(wtbs => res.json(wtbs));
});

app.get('/api/customers', (req,res) => {
  Customer.findAll().then(customers => res.json(customers));
});

app.get('/api/faqs', (req,res) => {
  Faq.findAll().then(faqs => res.json(faqs));
});

// app.get('/api/faq/:product_name?', (req,res) => {
//   let query;
//   if(req.params.product_name){
//     query = Faq.findAll({ include: [
//       { model: Product, where: {name: req.params.product_name} }
//     ]});
//   } else {
//     query = Faq.findAll();
//   }
//   return query.then(faqs => res.json(faqs));
// });
