/*jshint esversion: 6 */
/* jshint node: true */
'use strict';
const Sequelize = require('sequelize');
const BrandModel = require('./models/brand');
const BrandUrlModel = require('./models/brand_url');
const BusinessModel = require('./models/business');
const RetailerModel = require('./models/retailer');
const WhereToBuyModel = require('./models/where_to_buy');
const MarketModel = require('./models/market');
const CustomerModel = require('./models/customer');
const FaqModel = require('./models/faq');
const ProductModel = require('./models/product');

// const sequelize = new Sequelize('dbname', 'user', 'password', {
const sequelize = new Sequelize('mga-sandbox', 'local', 'test123', {
  host: 'localhost',
  dialect: 'mysql',
  pool: {
    max: 10,
    min: 0,
    acquire: 30000,
    idle: 10000
  }
});

const Brand = BrandModel(sequelize, Sequelize);
const BrandUrl = BrandUrlModel(sequelize, Sequelize);
const Business = BusinessModel(sequelize, Sequelize);
const Market = MarketModel(sequelize, Sequelize);
const Retailer = RetailerModel(sequelize, Sequelize);
const WhereToBuy = WhereToBuyModel(sequelize, Sequelize);
const Customer = CustomerModel(sequelize, Sequelize);
const Faq = FaqModel(sequelize, Sequelize);
const Product = ProductModel(sequelize, Sequelize);

console.log(BrandUrl);

// BrandUrl.belongsTo(Brand);
BrandUrl.options.classMethods.associate;
Brand.hasMany(BrandUrl);
Brand.belongsTo(Business);
Business.hasMany(Brand);
// Brand.hasMany(WhereToBuy);
WhereToBuy.belongsTo(Retailer);
WhereToBuy.belongsTo(Market);
WhereToBuy.belongsTo(Brand);
// Market.hasMany(Retailer);
Faq.belongsTo(Product);

//destroys and creates db
// sequelize.sync();
// // sequelize.sync({force : true })
// //  .then(() => {
// //    console.log(`Database & tables created!`)
// //  })

module.exports = {
  Brand,
  BrandUrl,
  Business,
  Market,
  WhereToBuy,
  Retailer,
  Customer,
  Faq,
  Product
};
