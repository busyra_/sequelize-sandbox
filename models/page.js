/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  return sequelize.define('page', {
    id: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    name: {
      type: DataTypes.STRING(265),
      allowNull: false
    },
    content: {
      type: DataTypes.TEXT,
      allowNull: true
    },
    title: {
      type: DataTypes.STRING(265),
      allowNull: false
    },
    description: {
      type: DataTypes.STRING(265),
      allowNull: false
    },
    keywords: {
      type: DataTypes.STRING(265),
      allowNull: false
    },
    created_user: {
      type: DataTypes.STRING(265),
      allowNull: false,
      defaultValue: 'web'
    },
    created_date: {
      type: DataTypes.DATE,
      allowNull: false,
      defaultValue: sequelize.literal('CURRENT_TIMESTAMP')
    },
    updated_user: {
      type: DataTypes.STRING(265),
      allowNull: false,
      defaultValue: 'web'
    },
    updated_date: {
      type: DataTypes.DATE,
      allowNull: false,
      defaultValue: sequelize.literal('CURRENT_TIMESTAMP')
    }
  }, {
    tableName: 'page'
  });
};
