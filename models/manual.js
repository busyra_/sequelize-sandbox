/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  return sequelize.define('manual', {
    id: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    product_id: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      primaryKey: true,
      references: {
        model: 'product',
        key: 'id'
      }
    },
    image: {
      type: DataTypes.STRING(265),
      allowNull: false
    },
    file_name: {
      type: DataTypes.STRING(265),
      allowNull: false
    },
    active: {
      type: DataTypes.INTEGER(11),
      allowNull: false
    },
    created_user: {
      type: DataTypes.STRING(265),
      allowNull: false,
      defaultValue: 'web'
    },
    created_date: {
      type: DataTypes.DATE,
      allowNull: false,
      defaultValue: sequelize.literal('CURRENT_TIMESTAMP')
    },
    updated_user: {
      type: DataTypes.STRING(265),
      allowNull: false,
      defaultValue: 'web'
    },
    updated_date: {
      type: DataTypes.DATE,
      allowNull: false,
      defaultValue: sequelize.literal('CURRENT_TIMESTAMP')
    }
  }, {
    tableName: 'manual'
  });
};
