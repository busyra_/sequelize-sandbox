/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  var brand_url = sequelize.define('brand_url', {
    id: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    brand_id: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      primaryKey: true,
      references: {
        model: 'brand',
        key: 'id'
      }
    },
    url: {
      type: DataTypes.STRING(265),
      allowNull: false
    },
    type: {
      type: DataTypes.STRING(265),
      allowNull: false
    },
    title: {
      type: DataTypes.STRING(265),
      allowNull: false
    },
    active: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      defaultValue: '1'
    },
    created_user: {
      type: DataTypes.STRING(265),
      allowNull: false,
      defaultValue: 'web'
    },
    created_date: {
      type: DataTypes.DATE,
      allowNull: false,
      defaultValue: sequelize.literal('CURRENT_TIMESTAMP')
    },
    updated_user: {
      type: DataTypes.STRING(265),
      allowNull: false,
      defaultValue: 'web'
    },
    updated_date: {
      type: DataTypes.DATE,
      allowNull: false,
      defaultValue: sequelize.literal('CURRENT_TIMESTAMP')
    }
  }, {
    tableName: 'brand_url',
    timestamps: false,
    underscored: true,
    classMethods: {
      associate:function(models){
        console.log(models);
        brand_url.belongsTo(models.brands);
      }
    }
  });
  return brand_url;
};
