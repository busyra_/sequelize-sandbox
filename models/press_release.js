/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  return sequelize.define('press_release', {
    id: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    title: {
      type: DataTypes.STRING(265),
      allowNull: false
    },
    file: {
      type: DataTypes.STRING(265),
      allowNull: true
    },
    url: {
      type: DataTypes.STRING(265),
      allowNull: true
    },
    release_date: {
      type: DataTypes.DATE,
      allowNull: false,
      defaultValue: sequelize.literal('CURRENT_TIMESTAMP')
    },
    active: {
      type: DataTypes.INTEGER(11),
      allowNull: false
    },
    created_user: {
      type: DataTypes.STRING(265),
      allowNull: false,
      defaultValue: 'web'
    },
    created_date: {
      type: DataTypes.DATE,
      allowNull: false,
      defaultValue: sequelize.literal('CURRENT_TIMESTAMP')
    },
    updated_user: {
      type: DataTypes.STRING(265),
      allowNull: false,
      defaultValue: 'web'
    },
    updated_date: {
      type: DataTypes.DATE,
      allowNull: false,
      defaultValue: sequelize.literal('CURRENT_TIMESTAMP')
    }
  }, {
    tableName: 'press_release'
  });
};
