/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  return sequelize.define('where_to_buy', {
    id: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    brand_id: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      primaryKey: true,
      references: {
        model: 'brand',
        key: 'id'
      }
    },
    market_id: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      primaryKey: true,
      references: {
        model: 'market',
        key: 'id'
      }
    },
    retailer_id: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      primaryKey: true,
      references: {
        model: 'retailer',
        key: 'id'
      }
    },
    title: {
      type: DataTypes.STRING(265),
      allowNull: false
    },
    url: {
      type: DataTypes.STRING(1000),
      allowNull: true
    },
    in_store_only: {
      type: DataTypes.INTEGER(11),
      allowNull: false
    },
    active: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      defaultValue: '1'
    },
    created_user: {
      type: DataTypes.STRING(265),
      allowNull: false,
      defaultValue: 'web'
    },
    created_date: {
      type: DataTypes.DATE,
      allowNull: false,
      defaultValue: sequelize.literal('CURRENT_TIMESTAMP')
    },
    updated_user: {
      type: DataTypes.STRING(265),
      allowNull: false,
      defaultValue: 'web'
    },
    updated_date: {
      type: DataTypes.DATE,
      allowNull: false,
      defaultValue: sequelize.literal('CURRENT_TIMESTAMP')
    }
  }, {
    tableName: 'where_to_buy',
    timestamps: false,
    underscored: true
  });
};