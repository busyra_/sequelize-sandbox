/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  return sequelize.define('customer', {
    id: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    first_name: {
      type: DataTypes.STRING(265),
      allowNull: false
    },
    last_name: {
      type: DataTypes.STRING(265),
      allowNull: false
    },
    middle_name: {
      type: DataTypes.STRING(265),
      allowNull: true
    },
    address_1: {
      type: DataTypes.STRING(265),
      allowNull: true
    },
    address_2: {
      type: DataTypes.STRING(265),
      allowNull: true
    },
    city: {
      type: DataTypes.STRING(265),
      allowNull: true
    },
    state: {
      type: DataTypes.STRING(265),
      allowNull: true
    },
    zip_code: {
      type: DataTypes.STRING(45),
      allowNull: true
    },
    country: {
      type: DataTypes.STRING(265),
      allowNull: true
    },
    email: {
      type: DataTypes.STRING(265),
      allowNull: false
    },
    contact_email: {
      type: DataTypes.INTEGER(11),
      allowNull: false
    },
    phone_number: {
      type: DataTypes.STRING(265),
      allowNull: true
    },
    gender: {
      type: DataTypes.STRING(30),
      allowNull: true
    },
    birth_date: {
      type: DataTypes.DATE,
      allowNull: true
    },
    created_user: {
      type: DataTypes.STRING(265),
      allowNull: false,
      defaultValue: 'web'
    },
    created_date: {
      type: DataTypes.DATE,
      allowNull: false,
      defaultValue: sequelize.literal('CURRENT_TIMESTAMP')
    },
    updated_user: {
      type: DataTypes.STRING(265),
      allowNull: false,
      defaultValue: 'web'
    },
    updated_date: {
      type: DataTypes.DATE,
      allowNull: false,
      defaultValue: sequelize.literal('CURRENT_TIMESTAMP')
    }
  }, {
    tableName: 'customer',
    timestamps: false,
    underscored: true
  });
};
